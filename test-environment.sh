#/bin/bash

docker volume create --label backup-me test-backup
docker run -v test-backup:/data --name helper busybox
docker cp . helper:/data
docker rm helper